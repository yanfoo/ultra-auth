import type { FooAuthConfig } from './internals';
export declare function getRoutes<SessionType>(config: FooAuthConfig<SessionType>): {
    [x: string]: import("./internals").FooAuthApiRouteHandler<any> | import("./internals").FooAuthApiRouteHandler<unknown>;
};
//# sourceMappingURL=routes.d.ts.map