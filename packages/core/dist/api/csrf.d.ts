import type { FooAuthApiRoutes, FooAuthConfigRoutePrefix } from '../internals';
export declare function csrfRoutes<SessionType>(baseRoutes: FooAuthConfigRoutePrefix): FooAuthApiRoutes<SessionType>;
//# sourceMappingURL=csrf.d.ts.map