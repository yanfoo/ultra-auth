import type { FooAuthApiRoutes, FooAuthConfigRoutePrefix } from '../internals';
export declare function sessionRoutes<SessionType = any>(baseRoutes: FooAuthConfigRoutePrefix): FooAuthApiRoutes<SessionType>;
//# sourceMappingURL=session.d.ts.map