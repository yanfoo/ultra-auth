import type { FooAuthApiRoutes, FooAuthConfigRoutePrefix } from '../internals';
export declare function authRoutes<SessionType = any>(baseRoutes: FooAuthConfigRoutePrefix): FooAuthApiRoutes<SessionType>;
//# sourceMappingURL=auth.d.ts.map